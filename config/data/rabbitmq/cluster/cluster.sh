#参考文档：https://www.cnblogs.com/yloved/p/12868892.html

#1、生成rabbitmq配置
cluster/dist/rabbitmq.sh
cluster/ram/rabbitmq.sh

#2、启动容器
docker-compose -f docker-compose-cluster.yml up -d

#3.分别进入从节点容器（本文中从节点是 rabbitmq3,rabbitmq2），启动脚本
docker exec -it rabbitmq1 bash
chmod +777 /home/rabbitmq.sh
./home/rabbitmq.sh


#主从集群已经搭建好，查看集群状态
rabbitmqctl cluster_status
#主从集群的不足： 默认情况下，队列只位于主节点上，尽管他们可以从所有节点看到和访问，也就是说整个集群与主节点共存亡。
#因此，当主节点宕机时，无法进行自动的故障转移，下面的队列镜像集群可以解决这个问题。



#搭队列镜像集群
```
镜像集群就是在主从集群的基础上，添加相应策略，将主节点消息队列中的信息备份到其它节点上，主节点宕机时，对整个集群不产生影响，使集群可以高可用。

添加策略（可以在创建队列之前添加，也可以创建队列之后添加）
参考官方文档： https://www.rabbitmq.com/parameters.html#policies
策略模板（[]表示可选参数，<>表示必穿参数）

rabbitmqctl set_policy [-p <vhost>] [--priority <priority>] [--apply-to <apply-to>] <name> <pattern> <definition>

#参数说明
-p vhost	对指定的vhost进行设置
name	policy的名称
pattern	queue的匹配模式（正则表达式）
definition	镜像定义：包含三个部分ha-mode,ha-params,ha-sync-mode
ha-mode:指明镜像队列的模式。all: 集群中所有节点进行镜像；exactly:在指定个数节点进行镜像，节点个数由ha-params指定;nodes:在指定节点进行镜像，节点名称由ha-params指定
ha-params: ha-mode模式需要用到的参数
ha-sync-mode: 消息的同步方式（automatic,manual）
priority	policy的优先级，当有多个策略指定同一个队列时，优先级高的策略生效

#添加策略实例
rabbitmqctl set_policy ha-11 '^11' '{"ha-mode":"all","ha-sync-mode":"automatic"}'
说明：策略正则表达式为 "^" 表示匹配所有队列名称， ^11 :表示匹配hello开头的队列

#查看当前策略
rabbitmqctl list_policies
#删除策略
rabbitmqctl clear_policy ha-11
```
