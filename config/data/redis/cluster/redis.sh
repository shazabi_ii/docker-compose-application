for port in $(seq 8010 8015);\
do \
mkdir -p /home/redis-cluster/redis${port}/conf
touch /home/redis-cluster/redis${port}/conf/redis.conf
cat << EOF >> /home/redis-cluster/redis${port}/conf/redis.conf
port ${port}
bind 0.0.0.0
cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 5000
cluster-announce-ip 192.168.3.86
cluster-announce-port ${port}
cluster-announce-bus-port 1${port}
appendonly yes
EOF
done


#查看配置
cat /home/redis-cluster/redis801{0..5}/conf/redis.conf


for port in $(seq 8010 8015); \
do \
   docker run -it -d -p ${port}:${port} -p 1${port}:1${port} \
  --privileged=true -v /home/redis-cluster/redis${port}/conf/redis.conf:/usr/local/etc/redis/redis.conf \
  --privileged=true -v /home/redis-cluster/redis${port}/data:/data \
  --restart always --name redis${port} --net redis \
  --sysctl net.core.somaxconn=1024 redis redis-server /usr/local/etc/redis/redis.conf; \
done


#查看启动ip
docker network inspect redis-net | grep -i -E "name|ipv4address"


#查看容器
docker container ls
docker-compose ps


#进入任意一个容器，进行集群
docker exec -it redis8010 bash
cd /usr/local/bin/
redis-cli --cluster create 192.168.3.80:8010 192.168.3.80:8011 192.168.3.80:8012 192.168.3.80:8013 192.168.3.80:8014 192.168.3.80:8015 --cluster-replicas 1 --cluster-yes


#连接redis-cluster，并添加数据到redis
在节点8010中输入以下命令
docker exec -it redis8010 bash
redis-cli -c -h 192.168.3.83 -p 8010
cluster nodes
set name 'test'
get name 'test'

在节点8015中输入以下命令
docker exec -it redis8015 bash
redis-cli -c -h 192.168.3.80 -p 8015
get name 'test'
查看集群不同节点是否可以获取相同数据
