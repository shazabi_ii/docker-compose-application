# docker-compose应用

## 介绍
1、本文主要介绍docker-compose安装Redis、MySQL、MongoDB、RabbitMQ、Nacos、Nginx等服务

> docker镜像官网:  https://hub.docker.com/

> Redis中文官方网站：http://www.redis.cn/download.html

> MySQL官网：https://dev.mysql.com/doc/refman/5.7/en/docker-mysql-getting-started.html

> Nacos官网：https://github.com/alibaba/nacos

> Nacos中文：https://nacos.io/zh-cn/docs/what-is-nacos.html

> Nacos官网代码：https://github.com/nacos-group/nacos-docker.git

> RabbitMQ官网：https://www.rabbitmq.com/parameters.html

```
机器配置：
Linux       7.4.1708      3.10.0-693.2.2.el7.x86_64    192.168.0.80
docker      20.10.12      build e91ed57
docker-compose 1.29.2     build 5becea4c
Redis       6.0           
MySQL       5.7          
MongoDB     5.0.1            
RabbitMQ    3.8.19-management
Nacos       1.4.0/2.0.3
Nginx       1.14.0

```


## docker
> 详细使用参考：https://starsky.blog.csdn.net/article/details/118992099

> 官网：https://docs.docker.com/engine/install/centos/

### docker安装
>安装步骤：
```
1、卸载旧版本
旧版本的 Docker 被称为docker或docker-engine。如果安装了这些，请卸载它们以及相关的依赖项。

 sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
如果yum报告没有安装这些软件包，那也没关系，docker的内容/var/lib/docker/，包括图像、容器、卷和网络，将被保留。Docker 引擎包现在被称为docker-ce
2、设置存储库
安装yum-utils包（提供yum-config-manager 实用程序）并设置稳定存储库
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
阿里云：
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
更新索引：yum makecache fast

3、安装 Docker 引擎
安装最新版本的 Docker Engine 和 containerd，或者进入下一步安装特定版本
sudo yum install docker-ce docker-ce-cli containerd.io
启动 Docker：sudo systemctl start docker

4、卸载 Docker Engine、CLI 和 Containerd 包：
 sudo yum remove docker-ce docker-ce-cli containerd.io
主机上的映像、容器、卷或自定义配置文件不会自动删除。删除所有镜像、容器和卷：
 sudo rm -rf /var/lib/docker
 sudo rm -rf /var/lib/containerd
您必须手动删除任何已编辑的配置文件。
```
### docker镜像仓库加速配置
> 参考：https://starsky.blog.csdn.net/article/details/121802957

```
登录阿里云官网：https://www.aliyun.com/?utm_content=se_1008364713
登录可以选择支付宝、微博邮箱、钉钉、淘宝等账号登录，登录后点击右上角 控制台-》进入到控制台管理页面-》选择镜像工具-》镜像加速配置-》选择对应的操作系统，安装提示说明配置即可。

1. 安装／升级Docker客户端
推荐安装1.10.0以上版本的Docker客户端，参考文档docker-ce
2. 配置镜像加速器
针对Docker客户端版本大于 1.10.0 的用户
您可以通过修改daemon配置文件/etc/docker/daemon.json来使用加速器
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://tnocr1gv.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker

```

### docker安全设置
由于docker使用2375端口，且没有任何加密和认证过程，一般内网使用，如果需要外网使用，则需要增加https认证机制，负责容易被攻击，甚至服务器瘫痪
> 参考：https://starsky.blog.csdn.net/article/details/118992099




## docker-compose
> 官网：https://docs.docker.com/compose/install/

### compose安装
```
安装步骤：
1、运行此命令以下载 Docker Compose 的当前稳定版本：
 sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
国内镜像：
curl -L https://get.daocloud.io/docker/compose/releases/download/1.29.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
2、对二进制文件应用可执行权限：
sudo chmod +x /usr/local/bin/docker-compose
3、建立软连接
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
4、测试安装
docker-compose --version

4、启动/停止
启动：docker-compose up
停止：docker-compose stop

5、卸载
sudo rm /usr/local/bin/docker-compose
如果您使用pip以下命令安装，则卸载 Docker Compose ：
 pip uninstall docker-compose

```

### 模版文件定义
> 官网：https://docs.docker.com/compose/compose-file/compose-file-v3/

### 基本命令使用
> 官网：https://docs.docker.com/compose/reference/
> 中文：https://vuepress.mirror.docker-practice.com/compose/commands/#down
``` 
sudo docker-compose --help
使用Docker定义和运行多容器应用程序.
Usage:
  docker-compose [-f <arg>...] [--profile <name>...] [options] [--] [COMMAND] [ARGS...]
  docker-compose -h|--help

Options:
  -f, --file FILE             指定compose文件(默认名称: docker-compose.yml,可以不用指定，如果是其他文件名，需要手动指定)
  -p, --project-name NAME     指定备用项目名称，(默认为当前目录名称)
  --profile NAME              指定要启用的配置文件
  -c, --context NAME       指定上下文名称
  --verbose                     显示输出信息
  --log-level LEVEL          设置日志级别(DEBUG, INFO, WARNING, ERROR, CRITICAL)
  --ansi (never|always|auto)  控制何时打印ANSI控制字符
  --no-ansi                   不打印ANSI控制字符（已弃用）
  -v, --version               打印版本信息并退出
  -H, --host HOST          连接到的守护进程套接字
  --tls                       Use TLS; implied by --tlsverify
  --tlscacert CA_PATH         CA签署的信任证书路径
  --tlscert CLIENT_CERT_PATH  TLS证书文件的路径
  --tlskey TLS_KEY_PATH       TLS密钥文件的路径
  --tlsverify                 使用TLS并验证远程
  --skip-hostname-check       不用根据客户端证书中指定的名称检查守护程序的主机名
  --project-directory PATH    指定备用工作目录(默认值：compose文件的路径)
  --compatibility             如果设置，Compose将尝试将v3文件中的密钥转换为其非Swarm等效项（已弃用）
  --env-file PATH             指定环境文件

Commands:
  build              生成或重建服务
  config             校验compose文件
  create             创建服务
  down               停止并删除服务
  events             从容器接收实时事件
  exec               在运行的容器中执行命令
  help               查看帮助命令
  images             查看所有镜像
  kill               结束运行容器
  logs               查看容器运行日志
  pause              暂停服务
  port               打印端口绑定的公共端口
  ps                 查看启动的容器
  pull               拉取服务镜像
  push               上传服务镜像
  restart            重启服务
  rm                 移除停止的容器
  run                运行命令
  scale              设置服务的容器数，即启动几个容器副本
  start              启动服务
  stop               停止服务
  top                显示正在运行的进程
  unpause            取消暂停服务
  up                 创建并启动容器
  version            显示版本信息并退出
[root@localhost demo]# 
```

#### images
``` 
 sudo docker-compose images --help
列出所创建容器使用的图像。该命令相当于docker images
Usage: images [options] [--] [SERVICE...]
Options:
    -q, --quiet  只显示容器IDs
[root@localhost test]# 
1、示例
查看所有镜像：sudo docker-compose images 或者 sudo docker-compose -f docker-compose.yml images; 
 默认查询镜像时，在当前目录下必须有docker-compose.yml文件。
只显示容器ID:  sudo docker-compose images -q
```

#### pull
```
sudo docker-compose -f docker-compose.yml pull --help
拉取服务依赖的镜像，但是不启动容器。该命令相当于docker pull
Usage: pull [options] [--] [SERVICE...]
Options:
    --ignore-pull-failures  拉取镜像，忽略失败的镜像
    --parallel              不推荐，并行拉取多个图像（默认情况下启用）.
    --no-parallel           禁用并行拉取镜像.
    -q, --quiet             拉取时不打印进度信息
    --include-deps          拉取声明为依赖项的服务
1、示例
docker-compose.yml文件：

拉取依赖服务：sudo docker-compose -f docker-compose.yml pull redis
查看镜像：sudo docker-compose images;
```

#### push
```
上传服务镜像.
sudo docker-compose -f docker-compose.yml push --help
上传服务镜像。该命令相当于docker push
Usage: push [options] [--] [SERVICE...]
Options:
    --ignore-push-failures  忽略上传镜像过程中的错误信息.
 sudo docker-compose -f docker-compose.yml push demo
```

#### build
```
sudo docker-compose build --help
编译打包生成镜像。该命令相当于docker build
Usage: build [options] [--build-arg key=val...] [--] [SERVICE...]
Options:
    --build-arg key=val     设置服务的构建时变量，相当于docker run -e
    --compress              使用gzip压缩构建上下文
    --force-rm              删除构建过程中的中间容器
    -m, --memory MEM        设置生成容器的内存限制
    --no-cache              生成映像时不要使用缓存
    --no-rm                 生成完成后不擅长中间容器
    --parallel              并行构建镜像
    --progress string       设置进度输出的类型（自动、普通、tty）
    --pull                  尝试拉取镜像的最新版本
    -q, --quiet             不用将任何内容打印到标准输出 
1、示例
编写docker-compose.yml文件
version: "3.8"
services:
  demo:
#启动服务时先将build命令中指定的dockerfile打包成镜像，在运行镜像
    build:
#指定上下文目录，即dockerfile所在目录，默认是docker-compose目录
      context: demo
      dockerfile: Dockerfile
    container_name: demo
    ports:
      - "8090:8090"
    networks:
      - mynet

#创建网桥
networks:
  mynet:

构建镜像
 sudo docker-compose -f docker-compose.yml build 
```

#### config
```
sudo docker-compose config --help
校验compose模版文件.
Usage: config [options]
Options:
    --resolve-image-digests  将图像标记固定到摘要
    --no-interpolate         不要插入临时环境变量
    -q, --quiet              只验证配置，不打印内容             
    --profiles               打印配置文件名称，每行一个
    --services               打印服务名称，每行一个.
    --volumes                打印挂载名称，每行一个.
    --hash="*"              打印服务配置哈希，每行一个。为指定服务的列表设置“service1，service2”，或使用通配符显示所有服务
1、示例
查看服务名：sudo docker-compose -f docker-compose.yml config --services
查看挂载目录：sudo docker-compose -f docker-compose.yml config --volumes

校验文件语法：sudo docker-compose -f docker-compose.yml config
```

#### ps
```
查看运行容器。该命令相当于docker ps
Usage: ps [options] [--] [SERVICE...]
Options:
    -q, --quiet          只查看容器ID
    --services           查看服务
    --filter KEY=VAL  查看服务中指定的属性
    -a, --all            查看所有容器(包含停止、运行、正在创建等等)
只查看容器ID： sudo docker-compose ps -q
查看服务：sudo docker-compose ps --services
查看服务中指定的属性：sudo docker-compose ps --filter "name=redis"
查看所有容器：sudo docker-compose ps -a
```

#### create
```
sudo docker-compose create --help
创建服务，此命令已弃用。改为使用带有“--no start”的“up”命令.
Usage: create [options] [SERVICE...]
Options:
    --force-recreate       即使容器的配置和映像没有更改，也要重新创建容器
    --no-recreate          如果容器已经存在，请不要重新创建。
    --no-build             即使它丢失了，也不创建镜像，
    --build                在创建容器之前构建镜像
1、示例
sudo docker-compose -f docker-compose.yml create --build
```


#### up
```
创建并在前台启动容器.该中方式启动不能在后台运行，退出后自动结束。该命令相当于docker run -it xxx
docker-compose up [options] [--scale SERVICE=NUM...] [SERVICE...]
选项包括：
-d                             在后台运行服务容器
–no-color                  不使用颜色来区分不同的服务的控制输出
–no-deps                  不启动服务所链接的容器
–force-recreate         强制重新创建容器，不能与–no-recreate同时使用
–no-recreate             如果容器已经存在，则不重新创建，不能与–force-recreate同时使用
–no-build                  不自动构建缺失的服务镜像
–build                       在启动容器前构建服务镜像
–abort-on-container-exit      停止所有容器，如果任何一个容器被停止，不能与-d同时使用
-t, –timeout TIMEOUT          停止容器时候的超时（默认为10秒）
–remove-orphans                 删除服务中没有在compose文件中定义的容器
–scale SERVICE=NUM          设置服务运行容器的个数，将覆盖在compose中通过scale指定的参数
1、示例
启动容器，并拉取依赖容器。
sudo docker-compose -f docker-compose.yml up
```

#### run
```
sudo docker-compose -f docker-compose.yml run --help
在服务中运行命令.
示例: $ docker-compose run web python manage.py shell
默认情况下，链接服务将启动，除非它们已经在运行。如果不想启动链接服务，请使用
`docker-compose run --no-deps SERVICE COMMAND [ARGS...]`.
Usage:
    run [options] [-v VOLUME...] [-p PORT...] [-e KEY=VAL...] [-l KEY=VALUE...] [--]
        SERVICE [COMMAND] [ARGS...]
Options:
    -d, --detach          独立模式: 在后台运行容器，打印新的容器名称.
    --name NAME           为容器指定一个名称
    --entrypoint CMD      覆盖容器启动入库命令
    -e KEY=VAL            设置环境变量(可多次使用)
    -l, --label KEY=VAL   添加或替代标签 (可多次使用)
    -u, --user=""         以指定的用户名或uid运行
    --no-deps             不要启动链接服务
    --rm                  运行后移除容器。在分离模式下忽略.
    -p, --publish=[]      将容器的端口发布到主机
    --service-ports       在启用服务端口并映射到主机的情况下运行命令.
    --use-aliases         在容器连接到的网络中使用服务的网络别名
    -v, --volume=[]       绑定并挂载卷名 (默认 [])
    -T                    禁用伪tty分配。默认情况下，`docker compose run`分配TTY
-w, --workdir=""      容器内的工作目录
1、示例
#在指定容器上执行一个ping命令：
sudo docker-compose -f docker-compose.yml run demo ping www.baidu.com
```

#### start
```
sudo docker-compose start --help
启动停止的容器。该命令相当于docker start [容器ID/容器名称]
Usage: start [服务名...]
1、示例
sudo docker-compose start tomcat01
```

#### restart
```
重启服务。该命令相当于docker restart [容器ID/容器名称]
sudo docker-compose -f docker-compose.yml restart --help
重新启动容器.
Usage: restart [options] [--] [SERVICE...]
Options:
  -t, --timeout TIMEOUT      以秒为单位指定关机超时.(默认: 10)
1、示例
重新启动demo服务。
sudo docker-compose -f docker-compose.yml restart -t 5 demo
```

#### exec
```
sudo docker-compose exec --help
在运行的容器中执行命令。该命令相当于docker exec [容器ID/容器名称]
Usage: exec [options] [-e KEY=VAL...] [--] SERVICE COMMAND [ARGS...]
Options:
    -d, --detach      分离模式：在后台运行命令
    --privileged     为进程授予扩展权限
    -u, --user USER   指定运行命令用户.
    -T                禁用伪tty分配。默认情况下，`docker compose exec`分配TTY
    --index=index     果服务有多个实例，则容器的索引[默认值：1]
    -e, --env KEY=VAL 设置环境变量（可多次使用，API<1.25不支持）
    -w, --workdir 此命令的workdir目录的DIR路径

```

#### down
```
 此命令将会停止 up 命令所启动的容器，并移除网络.
 sudo docker-compose down --help
停止容器并删除由“up”创建的容器、网络、卷和映像。默认情况下，仅删除以下内容:
- compose文件中服务定义的容器。
- compose文件中定义的网桥
- 已使用默认的网络
永远不会删除定义为“外部”的网络和卷.
Usage: down [options]
Options:
    --rmi type              删除镜像，必须是以下类型之一：all: 删除所有服务使用的镜像；local: 只删除本地自定义镜像标签的镜像。
    -v, --volumes           删除在挂载点定义的卷名和附加到容器中的卷名
    --remove-orphans        删除Compose文件中未定义的服务的容器
    -t, --timeout TIMEOUT   以秒为单位指定关机超时。（默认值：10）
[root@localhost test]# 
1、示例
sudo docker-compose down

sudo docker-compose down --rmi all
```

#### stop
```
sudo docker-compose stop --help
停止运行容器而不删除它，可以使用 `docker-compose start` 重新启动。
Usage: stop [options] [--] [SERVICE...]
Options:
  -t, --timeout TIMEOUT      以秒为单位指定关机超时.(默认: 10)
1、示例
sudo docker-compose stop demo
```

#### pause
```
sudo docker-compose pause --help
暂停一个服务容器。
Usage: pause [SERVICE...]
1、示例
sudo docker-compose pause tomcat01
```

#### unpause
```
sudo docker-compose unpause --help
取消暂停服务.
Usage: unpause [SERVICE...]
1、示例
sudo docker-compose unpause tomcat01
```

#### kill
```
sudo docker-compose kill --help
强制停止容器.
Usage: kill [options] [--] [SERVICE...]
Options:
    -s SIGNAL         通过发送 SIGKILL 信号来强制停止服务容器.默认信号使用SIGKILL.
1、示例
sudo docker-compose kill -s SIGINT
```

#### logs
```
sudo docker-compose logs --hlep
查看服务容器的输出日志。该命令相当于docker logs 
Usage: logs [options] [--] [SERVICE...]
Options:
    --no-color              产生单色输出
    -f, --follow            跟踪日志输出.
    -t, --timestamps   显示时间戳.
    --tail="all"          从每个容器的日志末尾开始显示的行数.
    --no-log-prefix    不要在日志中打印前缀.
1、示例
sudo docker-compose logs tomcat01 

不显示日志颜色：sudo docker-compose logs --no-color tomcat01
跟踪日志输出：sudo docker-compose logs -f tomcat01
查看最后5行日志：sudo docker-compose logs --tail 5 tomcat01
```


#### rm
```
sudo docker-compose rm --help
删除服务容器.默认情况下，不会删除附加到容器的匿名卷。您可以使用“-v”覆盖此选项。要列出所有卷，请使用“docker volume ls”。任何不在卷中的数据都将丢失.
Usage: rm [options] [--] [SERVICE...]
Options:
    -f, --force   强制删除，不用确认.
    -s, --stop    先停止容器，在删除容器。
    -v            删除附加到容器的所有匿名卷
    -a, --all     已弃用-无效
1、示例
删除运行的容器： sudo docker rm -f $(sudo docker ps -q)
删除镜像： sudo docker rmi $(sudo docker images -q)
查看运行容器：sudo docker-compose ps
停止并删除：sudo docker-compose rm -s demo

删除所有容器：sudo docker-compose rm -s $(sudo docker-compose ps --services)
```

#### events
```
从容器接收实时事件。
sudo docker-compose events --help
从容器接收实时事件.
Usage: events [options] [--] [SERVICE...]
Options:
    --json      将事件作为json对象流输出
1、示例
sudo docker-compose events demo
```

#### port
```
sudo docker-compose port --help
打印端口绑定的公共端口.
Usage: port [options] [--] SERVICE PRIVATE_PORT
Options:
    --protocol=proto  指定端口协议tcp/udp [默认: tcp]
    --index=index     如果服务有多个实例，则容器的索引[默认值：1]
1、示例
```

#### scale
```
设置服务的容器数，即启动几个容器副本.
sudo docker-compose scale --help
设置指定服务运行的容器个数，通过service=num的参数来设置数量。
示例: $ docker-compose scale web=2 worker=3
此命令已弃用。使用带有“---scale”标志的up命令相反.
Usage: scale [options] [SERVICE=NUM...]
Options:
  -t, --timeout TIMEOUT      以秒为单位指定关机超时.(默认: 10)

1、示例
启动多个容器。docker-compose.yml文件内容：

通过scale扩展副本时，不能指定绑定端口，容器名称，这样造成启动时容器名称、端口被占用。
sudo docker-compose scale demo=2
```

#### top
```
显示正在运行的进程.
sudo docker-compose top --help
查看容器运行的进程
Usage: top [SERVICE...]
1、示例
查看demo容器进程：sudo docker-compose top demo
```

#### version
```
显示版本信息并退出。
sudo docker-compose version --help
显示版本信息并退出.
Usage: version [--short]
Options:
--short     仅显示Compose的版本号
1、示例
sudo docker-compose version --short

```




## Swarm
> https://docs.docker.com/engine/swarm/

> 要在 swarm 模式下使用 Docker，请安装 Docker。请参阅 所有操作系统和平台的安装说明。
  当前版本的 Docker 包括swarm 模式，用于本地管理称为swarm的 Docker 引擎集群。使用 Docker CLI 创建群、将应用程序服务部署到群以及管理群行为。
  Docker Swarm 模式内置于 Docker 引擎中。不要将 Docker Swarm 模式与 不再积极开发的Docker Classic Swarm混淆

> 功能亮点
``` 
与 Docker Engine 集成的集群管理：使用 Docker Engine CLI 创建一组 Docker Engine，您可以在其中部署应用程序服务。您不需要额外的编排软件来创建或管理集群。

去中心化设计： Docker 引擎不是在部署时处理节点角色之间的差异，而是在运行时处理任何专业化。您可以使用 Docker 引擎部署两种类型的节点、管理器和工作器。这意味着您可以从单个磁盘映像构建整个集群。

声明式服务模型： Docker Engine 使用声明式方法让您定义应用程序堆栈中各种服务的所需状态。例如，您可能会描述一个应用程序，该应用程序由具有消息队列服务和数据库后端的 Web 前端服务组成。

缩放：对于每个服务，您可以声明要运行的任务数量。当您扩大或缩小规模时，群管理器会通过添加或删除任务来自动适应以保持所需的状态。

期望状态协调： swarm 管理器节点不断监控集群状态，并协调实际状态和您表达的期望状态之间的任何差异。例如，如果您设置一个服务来运行容器的 10 个副本，并且托管其中两个副本的工作机器崩溃，则管理器会创建两个新副本来替换崩溃的副本。swarm manager 将新的副本分配给正在运行且可用的 worker。

多主机网络：您可以为您的服务指定一个覆盖网络。群管理器在初始化或更新应用程序时自动为覆盖网络上的容器分配地址。

服务发现： Swarm 管理器节点为 Swarm 中的每个服务分配一个唯一的 DNS 名称并平衡运行容器的负载。您可以通过嵌入在 swarm 中的 DNS 服务器查询在 swarm 中运行的每个容器。

负载均衡：您可以将服务的端口暴露给外部负载均衡器。在内部，swarm 允许您指定如何在节点之间分发服务容器。

默认安全： swarm 中的每个节点都强制执行 TLS 相互身份验证和加密，以保护自身与所有其他节点之间的通信。您可以选择使用自签名根证书或来自自定义根 CA 的证书。

滚动更新：在推出时，您可以增量地将服务更新应用到节点。swarm 管理器允许您控制服务部署到不同节点集之间的延迟。如果出现任何问题，您可以回滚到该服务的先前版本。
```

### Swarm集群
> 官网：https://docs.docker.com/engine/swarm/

#### Swarm集群条件
```
三个可以通过网络通信的 Linux 主机，安装了 Docker
管理员机器的IP地址
打开主机之间的端口
用于集群管理通信的TCP 端口 2377
用于节点间通信的TCP和UDP 端口 7946
UDP 端口 4789用于覆盖网络流量
需要三个安装了 Docker 并且可以通过网络进行通信的 Linux 主机。这些可以是物理机、虚拟机、Amazon EC2 实例或以其他方式托管。
其中一台机器是经理（称为manager1），其中两台是工人（worker1和worker2）
```

#### 创建管理节点
```
docker swarm init --advertise-addr 192.168.3.85
[root@localhost ~]#  docker swarm init --advertise-addr 192.168.3.85
Swarm initialized: current node (uhqsbzo5s3i3i3nhfr4o7m3gu) is now a manager.
To add a worker to this swarm, run the following command:
    docker swarm join --token SWMTKN-1-2o01hzyxquaiodr27h40obm66i906dhfvp002rnfm9isr5okkf-6n2ux88advdj2jqwkbb3wp7sj 192.168.3.85:2377
To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
[root@localhost ~]# 

```
#### 将worker加入主节点
```
通过命令docker swarm join-token worker ，查询加入管理节点令牌。
然后加入管理节点。
    docker swarm join --token SWMTKN-1-2o01hzyxquaiodr27h40obm66i906dhfvp002rnfm9isr5okkf-6n2ux88advdj2jqwkbb3wp7sj 192.168.3.85:2377
```

#### 加入管理节点
```
通过命令docker swarm join-token manager ，查询加入管理节点令牌。
然后加入管理节点。
    docker swarm join --token SWMTKN-1-2o01hzyxquaiodr27h40obm66i906dhfvp002rnfm9isr5okkf-6n2ux88advdj2jqwkbb3wp7sj 192.168.3.85:2377
```

#### 查看集群信息
```
注意，集群的操作都是在管理节点上，登录管理节点，查看集群信息
docker node ls

```

### Service管理服务
``` 
sudo docker service --help
Usage:  docker service COMMAND
管理服务
Commands:
  create      创建服务
  inspect     查看服务信息
  logs        获取服务或任务的日志
  ls          查看所有服务
  ps          列出一个或多个服务的任务
  rm          删除服务
  rollback    恢复对服务配置的更改
  scale       扩展一个或多个复制的服务
  update      更新服务
Run 'docker service COMMAND --help' for more information on a command.
[root@localhost ~]# 
1.create创建服务
 sudo docker service create --help
Usage:  docker service create [OPTIONS] IMAGE [COMMAND] [ARG...]
Create a new service
Options:
      --cap-add list                       添加Linux功能
      --cap-drop list                      删除Linux功能
      --config config                      指定要向服务公开的配置
      --constraint list                    布局约束
      --container-label list               容器标签
      --credential-spec credential-spec    托管服务帐户的凭据规范（仅限Windows）
  -d, --detach                             立即退出，而不是等待服务聚合
      --dns list                          设置自定义DNS服务
      --dns-option list                    设置DNS选项
      --dns-search list                    设置自定义DNS搜索域
      --endpoint-mode string               端点模式（vip或dnsrr）（默认为“vip”）
      --entrypoint command                 覆盖镜像的默认入口点
  -e, --env list                           设置环境变量
      --env-file list                      通过文件读入环境变量
      --generic-resource list              用户定义的资源
      --group list                         为容器设置一个或多个补充用户组
      --health-cmd string                  运行命令检查状况
      --health-interval duration           设置健康检查时间 (ms|s|m|h)
      --health-retries int                 需要报告连续故障
      --health-start-period duration       计算重试次数之前容器初始化的开始时间(ms|s|m|h)
      --health-timeout duration            允许运行一次检查的最长时间(ms|s|m|h)
      --host list                          设置一个或多个自定义主机到IP映射（主机：IP）
      --hostname string                    设置容器主机名
      --init                               在每个服务容器中使用init来转发信号和获取进程
      --isolation string                   服务容器隔离模式
  -l, --label list                         设置服务标签
      --limit-cpu decimal                  限制 CPUs
      --limit-memory bytes                 限制 内存
      --limit-pids int                     限制最大进程数(default 0 = unlimited)
      --log-driver string                  服务日志驱动程序
      --log-opt list                       服务日志驱动选项
      --max-concurrent uint                要同时运行的作业任务数(默认为副本--replicas)
      --mode string                        设置服务模式 (replicated, global, replicated-job, or global-job) (default "replicated")
      --mount mount                        将文件系统装载附加到服务
      --name string                        设置服务名
      --network network                    设置网络
      --no-healthcheck                     禁用任何指定的容器 HEALTHCHECK
      --no-resolve-image                   不要查询注册表以解析图像摘要和支持的平台
      --placement-pref pref                添加放置首选项
  -p, --publish port                       将端口发布为节点端口
  -q, --quiet                              抑制进度输出
      --read-only                          以只读方式装载容器的根文件系统
      --replicas uint                      设置任务副本数
      --replicas-max-per-node uint         Maximum number of tasks per node (default 0 = unlimited)
      --reserve-cpu decimal                Reserve CPUs
      --reserve-memory bytes               Reserve Memory
      --restart-condition string           Restart when condition is met ("none"|"on-failure"|"any") (default "any")
      --restart-delay duration             Delay between restart attempts (ns|us|ms|s|m|h) (default 5s)
      --restart-max-attempts uint          Maximum number of restarts before giving up
      --restart-window duration            Window used to evaluate the restart policy (ns|us|ms|s|m|h)
      --rollback-delay duration            Delay between task rollbacks (ns|us|ms|s|m|h) (default 0s)
      --rollback-failure-action string     Action on rollback failure ("pause"|"continue") (default "pause")
      --rollback-max-failure-ratio float   Failure rate to tolerate during a rollback (default 0)
      --rollback-monitor duration          Duration after each task rollback to monitor for failure (ns|us|ms|s|m|h) (default 5s)
      --rollback-order string              Rollback order ("start-first"|"stop-first") (default "stop-first")
      --rollback-parallelism uint          Maximum number of tasks rolled back simultaneously (0 to roll back all at once)
                                           (default 1)
      --secret secret                      Specify secrets to expose to the service
      --stop-grace-period duration         Time to wait before force killing a container (ns|us|ms|s|m|h) (default 10s)
      --stop-signal string                 Signal to stop the container
      --sysctl list                        Sysctl options
  -t, --tty                                Allocate a pseudo-TTY
      --ulimit ulimit                      Ulimit options (default [])
      --update-delay duration              更新延迟时间 (ns|us|ms|s|m|h) (default 0s)
      --update-failure-action string       Action on update failure ("pause"|"continue"|"rollback") (default "pause")
      --update-max-failure-ratio float     Failure rate to tolerate during an update (default 0)
      --update-monitor duration            Duration after each task update to monitor for failure (ns|us|ms|s|m|h) (default 5s)
      --update-order string                Update order ("start-first"|"stop-first") (default "stop-first")
      --update-parallelism uint            Maximum number of tasks updated simultaneously (0 to update all at once) (default 1)
  -u, --user string                        指定用户ID(format: <name|uid>[:<group|gid>])
      --with-registry-auth                 向swarm代理发送注册表身份验证详细信息
  -w, --workdir string                     容器内的工作目录
[root@localhost ~]# 

1、示例
sudo docker service create --name my-nginx -p 8888:80 --hostname nginx --replicas 3 nginx 
创建nginx服务，指定名称为my-nginx ,端口为8888，主机名为nginx，副本数为3.

浏览器测试：
http://192.168.3.87:8888/
http://192.168.3.86:8888/
http://192.168.3.85:8888/
可以看到，3台机器都可以访问，我们停止任何一台服务测试。

2.inspect查看服务信息
[root@localhost ~]# sudo docker service inspect --help
Usage:  docker service inspect [OPTIONS] SERVICE [SERVICE...]
Display detailed information on one or more services
Options:
  -f, --format string   Format the output using the given Go template
      --pretty          Print the information in a human friendly format
[root@localhost ~]# 
1、示例
sudo docker service inspect my-nginx

3.logs查看服务日志
[root@localhost ~]# sudo docker service logs --help
Usage:  docker service logs [OPTIONS] SERVICE|TASK
Fetch the logs of a service or task
Options:
      --details        显示提供给日志的其他详细信息
  -f, --follow         跟踪日志输出
      --no-resolve     不要将ID映射到输出中的名称
      --no-task-ids    不要在输出中包含任务ID
      --no-trunc       不要截断输出
      --raw            不要格式化日志
      --since string   输出从xxx到xx的日志 (e.g. 2013-01-02T13:23:37Z) or relative (e.g. 42m for 42 minutes)
  -n, --tail string    从日志末尾显示的行数 (default "all")
  -t, --timestamps     显示时间戳
[root@localhost ~]# 
1、示例
sudo docker service logs  my-nginx

sudo docker service logs -n 5  my-nginx


4.ls查看所有服务
sudo docker service ls --help
Usage:  docker service ls [OPTIONS]
List services
Aliases:
  ls, list
Options:
  -f, --filter filter   根据提供的条件筛选输出
      --format string   使用go模版打印服务
  -q, --quiet           仅显示IDs
[root@localhost ~]# 
1、示例
sudo docker service ls 


5.ps列出所有运行的服务
[root@localhost ~]# sudo docker service ps --help
Usage:  docker service ps [OPTIONS] SERVICE [SERVICE...]
List the tasks of one or more services
Options:
  -f, --filter filter   Filter output based on conditions provided
      --format string   Pretty-print tasks using a Go template
      --no-resolve      Do not map IDs to Names
      --no-trunc        Do not truncate output
  -q, --quiet           Only display task IDs
[root@localhost ~]# 
1、示例
sudo docker service ps my-nginx

6.rm删除服务
sudo docker service rm my-nginx


7.  rollback恢复对服务配置的更改
[root@localhost ~]# sudo docker service rollback --help
Usage:  docker service rollback [OPTIONS] SERVICE
恢复对服务配置的更改
Options:
  -d, --detach   立即退出，而不是等待服务聚合
  -q, --quiet    抑制进度输出
[root@localhost ~]# 
8.  scale复制的服务
[root@localhost ~]# sudo docker service scale --help
Usage:  docker service scale SERVICE=REPLICAS [SERVICE=REPLICAS...]
扩展一个或多个复制的服务
Options:
  -d, --detach   立即退出，而不是等待服务聚合
[root@localhost ~]# 
1、示例
sudo docker service scale my-nginx=3


9.  update更新服务
 sudo docker service update --replicas 2 my-nginx
```




## Redis

### Redis单机版
#### 创建目录
```
删除目录：sudo rm -rf /home/data/
将data上传到服务器/home目录下。
授权：sudo chmod 777 -R /home/data
```
#### 校验compose配置文件
进入到/home/data目录下,检查docker-compose-redis.yml文件语法是否正确。
```
sudo docker-compose -f docker-compose-redis.yml config
```

#### 启动服务
```
删除所有运行镜像：sudo docker rm -f $(sudo docker ps -qa)
启动服务：sudo docker-compose -f docker-compose-redis.yml up -d
查看启动服务：sudo docker ps
```
所有服务都已经启动，状态都是up， 如果状态不为up, 需要排查启动问题。以上启动容器完成，下面需要测试各个容器是否工作正常。

#### 客户端链接测试
```
sudo docker exec -it redis bash
redis-cli
auth '123456'
或者使用远程登录：redis-cli -h 192.168.0.80 -p 6379 -a 123456
```

### Redis集群
> redis集群官网：https://redis.io/topics/cluster-tutorial

#### 创建reids配置
```
登录服务器，执行如下命令：
for port in $(seq 8010 8015);\
do \
mkdir -p /home/redis-cluster/redis${port}/conf
touch /home/redis-cluster/redis${port}/conf/redis.conf
cat << EOF >> /home/redis-cluster/redis${port}/conf/redis.conf
port ${port}
bind 0.0.0.0
cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 5000
cluster-announce-ip 192.168.3.86
cluster-announce-port ${port}
cluster-announce-bus-port 1${port}
appendonly yes
EOF
done

```

#### 启动服务
```
将data/reids/cluster/redis-cluster.yml文件上传到服务器。
删除所有运行镜像：sudo docker rm -f $(sudo docker ps -qa)
启动服务：sudo docker-compose -f redis-cluster up -d

#查看启动ip
docker network inspect redis-net | grep -i -E "name|ipv4address"

#查看容器
docker container ls
docker-compose ps

```

#### 集群
```
#进入任意一个容器，进行集群
docker exec -it redis8010 bash
cd /usr/local/bin/
redis-cli --cluster create 192.168.3.80:8010 192.168.3.80:8011 192.168.3.80:8012 192.168.3.80:8013 192.168.3.80:8014 192.168.3.80:8015 --cluster-replicas 1 --cluster-yes

注意：这里使用ip地址进行集群，如果是域名，可能报错，redis对域名支持不太友好导致。

```

#### 集群测试
```
#连接redis-cluster，并添加数据到redis
在节点8010中输入以下命令
docker exec -it redis8010 bash
redis-cli -c -h 192.168.3.83 -p 8010
cluster nodes
set name 'test'
get name 'test'

在节点8015中输入以下命令
docker exec -it redis8015 bash
redis-cli -c -h 192.168.3.80 -p 8015
get name 'test'
查看集群不同节点是否可以获取相同数据

```




## MySQL
>安装: https://dev.mysql.com/doc/refman/5.7/en/installing.html

> mysql环境变量: https://dev.mysql.com/doc/refman/5.7/en/environment-variables.html

### MySQL单机版

#### 创建目录
```
删除目录：sudo rm -rf /home/data/
将data上传到服务器/home目录下。
授权：sudo chmod 777 -R /home/data
```
#### 校验compose配置文件
进入到/home/data目录下,检查docker-compose-mysql.yml文件语法是否正确。
```
sudo docker-compose -f docker-compose-mysql.yml config
```

#### 启动服务
```
删除所有运行镜像：sudo docker rm -f $(sudo docker ps -qa)
启动服务：sudo docker-compose -f docker-compose-mysql.yml up -d
查看启动服务：sudo docker ps
```

#### 命令测试验证
```
登录容器：sudo docker exec -it mysql bash
登录mysql:  mysql -uroot -p123456
 mysql -h 192.168.0.80 -P3306 -uroot -p123456
查看数据库：show databases;
查看用户：select  user,host from mysql.user

查看root用户权限,这里的root支持所有ip用户访问（%表示支持所有ip）
select host,user from mysql.user;

如果root用户权限不足，则需要修改用登录权限：
use mysql #访问mysql库
update user set host = '%' where user = 'root'; #使root能再任何host访问
FLUSH PRIVILEGES; 

开启远程访问权限
命令：use mysql;
命令：select host,user from user;
命令：ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY '123456';
命令：flush privileges;

```

#### 远程登录测试验证
```
远程链接测试：mysql -h192.168.0.80 -P3306 -uroot -p123456

```


### MySQL集群
> 参考：https://blog.csdn.net/qq_42413011/article/details/115706921

#### 创建基本配置
```
将config/data/mysql/cluster目录的init目录及文件上传到服务器/home目录。
将config/data/mysql/cluster/docker-compose-cluster.yml文件上传到服务器/home目录。
```

#### 启动服务
```
sudo docker-compose -f docker-compose-cluster.yml up -d
查看主节点日志：sudo docker logs mysql-master

```
#### 查看同步状态
```
#1、登录master查看
sudo docker exec -it mysql-master bash
mysql -uroot -p123456
#查看状态:
show master status \G

mysql> show master status \G
*************************** 1. row ***************************
             File: mysql-bin.000003
         Position: 154
     Binlog_Do_DB:
 Binlog_Ignore_DB:
Executed_Gtid_Set:
1 row in set (0.00 sec)

mysql>


#2、登录slave1查看
sudo docker exec -it mysql-slave1 bash
mysql -uroot -p123456
#查看主从同步状态：
show slave status \G
#可以看到 Slave_IO_Running 和 Slave_SQL_Running 都是Yes，就说明主从同步已经配置成功。
#如果Slave_IO_Running为Connecting，SlaveSQLRunning为Yes，
#则说明配置有问题，这时候就要检查配置中哪一步出现问题了哦，可根据Last_IO_Error字段信息排错。

mysql> 
mysql> show slave status \G
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: mysqlmaster
                  Master_User: sync_admin
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000003
          Read_Master_Log_Pos: 154
               Relay_Log_File: mysqlslave1-relay-bin.000004
                Relay_Log_Pos: 320
        Relay_Master_Log_File: mysql-bin.000003
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: 
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 154
              Relay_Log_Space: 533
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 1
                  Master_UUID: 9b4f1d60-663d-11ec-86ce-0242ac180002
             Master_Info_File: /var/lib/mysql/master.info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
           Master_Retry_Count: 86400
                  Master_Bind: 
      Last_IO_Error_Timestamp: 
     Last_SQL_Error_Timestamp: 
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
           Retrieved_Gtid_Set: 
            Executed_Gtid_Set: 
                Auto_Position: 0
         Replicate_Rewrite_DB: 
                 Channel_Name: 
           Master_TLS_Version: 
1 row in set (0.00 sec)

mysql> 



#3、登录slave2查看
 sudo docker exec -it mysql-slave2 bash
 mysql -uroot -p123456
#查看slave2主从状态：
show slave status \G

mysql> 
mysql> show slave status \G
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: mysqlmaster
                  Master_User: sync_admin
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: mysql-bin.000003
          Read_Master_Log_Pos: 154
               Relay_Log_File: mysqlslave2-relay-bin.000003
                Relay_Log_Pos: 320
        Relay_Master_Log_File: mysql-bin.000003
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB: 
          Replicate_Ignore_DB: 
           Replicate_Do_Table: 
       Replicate_Ignore_Table: 
      Replicate_Wild_Do_Table: 
  Replicate_Wild_Ignore_Table: 
                   Last_Errno: 0
                   Last_Error: 
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 154
              Relay_Log_Space: 533
              Until_Condition: None
               Until_Log_File: 
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File: 
           Master_SSL_CA_Path: 
              Master_SSL_Cert: 
            Master_SSL_Cipher: 
               Master_SSL_Key: 
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error: 
               Last_SQL_Errno: 0
               Last_SQL_Error: 
  Replicate_Ignore_Server_Ids: 
             Master_Server_Id: 1
                  Master_UUID: 9b4f1d60-663d-11ec-86ce-0242ac180002
             Master_Info_File: /var/lib/mysql/master.info
                    SQL_Delay: 0
          SQL_Remaining_Delay: NULL
      Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
           Master_Retry_Count: 86400
                  Master_Bind: 
      Last_IO_Error_Timestamp: 
     Last_SQL_Error_Timestamp: 
               Master_SSL_Crl: 
           Master_SSL_Crlpath: 
           Retrieved_Gtid_Set: 
            Executed_Gtid_Set: 
                Auto_Position: 0
         Replicate_Rewrite_DB: 
                 Channel_Name: 
           Master_TLS_Version: 
1 row in set (0.00 sec)

mysql> 

```

#### 测试数据同步
```
#1、登录master创建数据库
docker exec -ti mysql-master bash
mysql -uroot -p123456
CREATE DATABASE `sync-data`;
USE `sync-data`;
CREATE TABLE `sync-data`(
id INT(10) AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(20) NOT NULL DEFAULT '' COMMENT '名称'
) ENGINE=INNODB;
INSERT INTO `sync-data`(`name`) VALUES('zhangsan'),('lisi');


#2.进入mysql-slave1，查看数据同步情况
docker exec -ti mysql-slave1 bash
mysql -uroot -p123456
show databases;
SELECT * FROM `sync-data`.`sync-data`;

#3.进入mysql-slave2，查看数据同步情况
docker exec -ti mysql-slave2 bash
mysql -uroot -p123456
show databases;
SELECT * FROM `sync-data`.`sync-data`;

```



## MongoDB

### MongoDB单机版

#### 创建目录
```
删除目录：sudo rm -rf /home/data/
将data上传到服务器/home目录下。
授权：sudo chmod 777 -R /home/data
```
#### 校验compose配置文件
进入到/home/data目录下,检查docker-compose-mongo.yml文件语法是否正确。
```
sudo docker-compose -f docker-compose-mongo.yml config
```

#### 启动服务
```
删除所有运行镜像：sudo docker rm -f $(sudo docker ps -qa)
启动服务：sudo docker-compose -f docker-compose-mongo.yml up -d
查看启动服务：sudo docker ps
```

#### 登录mongodb控制台
```
进入容器: sudo docker exec -it mongodb bash
登录mongodb:
mongo -u "admin" -p'123456' --authenticationDatabase "admin"
用户名密码为compose配置账号密码

查看数据库：show dbs;

```

#### 创建root账号
```
use beacon;
db.beacon.insert({"name":"测试创建数据库beacon"});
db.createUser({ user: 'root', pwd: '123456', roles: [ { role: "dbOwner", db: "beacon" } ] });
db.auth('root','123456');


命令说明：
user：用户名
pwd：密码
roles：指定用户的角色，可以用一个空数组给新用户设定空角色；在roles字段,可以指定内置角色和用户定义的角色。role里的角色可以选：
  Built-In Roles（内置角色）：
    1. 数据库用户角色：read、readWrite;
    2. 数据库管理角色：dbAdmin、dbOwner、userAdmin；
    3. 集群管理角色：clusterAdmin、clusterManager、clusterMonitor、hostManager；
    4. 备份恢复角色：backup、restore；
    5. 所有数据库角色：readAnyDatabase、readWriteAnyDatabase、userAdminAnyDatabase、dbAdminAnyDatabase
    6. 超级用户角色：root 
    // 这里还有几个角色间接或直接提供了系统超级用户的访问（dbOwner 、userAdmin、userAdminAnyDatabase）
    7. 内部角色：__system

具体角色：
Read：允许用户读取指定数据库
readWrite：允许用户读写指定数据库
dbAdmin：在当前的数据库中执行管理操作，如索引的创建、删除、统计、查看等
dbOwner：在当前的数据库中执行任意操作，增、删、改、查等
userAdmin：在当前的数据库中管理User，创建、删除和管理用户。

clusterAdmin：只在admin数据库中可用，赋予用户所有分片和复制集相关函数的管理权限。
clusterManager：授权管理和监控集群的权限
clusterMonoitor：授权监控集群的权限，对监控工具具有readonly的权限
hostManager：管理server
readAnyDatabase：只在admin数据库中可用，赋予用户所有数据库的读权限
readWriteAnyDatabase：只在admin数据库中可用，赋予用户所有数据库的读写权限
userAdminAnyDatabase：只在admin数据库中可用，赋予用户所有数据库的userAdmin权限
dbAdminAnyDatabase：只在admin数据库中可用，赋予用户所有数据库的dbAdmin权限。
root：只在admin数据库中可用。超级账号，超级权限。

```

#### 远程命令登录测试
使用刚创建的admin超级管理员用户登录。
```
docker exec -it mongodb /bin/bash
 mongo -host 192.168.0.80 -u "admin" -p'123456' --authenticationDatabase "admin" 
show dbs;
use admin;
show collections;
db.system.users.find();


参数说明：
--host: 登录mongodb服务器ip地址
-u: 登录用户
-p：登录密码
--authenticationDatabase: 授权数据库


2、使用刚创建的root用户登录。
 mongo --host 192.168.0.80 -u "root" -p'123456' --authenticationDatabase "beacon"
show dbs;
use beacon;
show collections;
db.beacon.find();


3、常用命令：
> use admin
db.system.users.remove({user:"root"}); //删除root用户
db.system.users.find(); 查看所有用户
 db.addUser("root","123456");//已经存在的用户更改密码
用户user02添加admin数据库的readWrite角色
> db.grantRolesToUser( "user02", [ { role: "readWrite", db: "admin" }, { role: "user02", db: "admin" } ] )

更新用户user02具有admin数据库readWrite角色为read角色。
> use admin
> db.updateUser( "user02", { customData: { info: "user for user02" }, roles: [ { role: "user02", db: "admin" }, { role: "read", db: "admin" } ] } )


```
#### Web端管理界面

```
mongo web端管理界面，浏览器访问：http://192.168.0.80:27018/， 账号密码为compose中配置。

```




## RabbitMQ
> 官网：https://www.rabbitmq.com/admin-guide.html

### RabbitMQ单机版

#### 创建目录
```
删除目录：sudo rm -rf /home/data/
将data上传到服务器/home目录下。
授权：sudo chmod 777 -R /home/data
```
#### 校验compose配置文件
进入到/home/data目录下,检查docker-compose-rabbitmq.yml文件语法是否正确。
```
sudo docker-compose -f docker-compose-rabbitmq.yml config
```

#### 启动服务
```
删除所有运行镜像：sudo docker rm -f $(sudo docker ps -qa)
启动服务：sudo docker-compose -f docker-compose-rabbitmq.yml up -d
查看启动服务：sudo docker ps
```

#### 登录mongodb控制台
```
sudo docker exec -it rabbitmq bash

浏览器访问：http://192.168.0.80:15672/#/queues，登录账号 guest guest

```

### RabbitMQ集群
> 官网：https://www.rabbitmq.com/clustering.html#creating





## Nacos
```
Docker镜像官网：https://github.com/nacos-group/nacos-docker
nacos官网：https://github.com/alibaba/nacos
Nacos中文：https://nacos.io/zh-cn/docs/what-is-nacos.html
Nacos官网代码：https://github.com/nacos-group/nacos-docker.git
```

### Nacos单机版

#### 创建目录
```
删除目录：sudo rm -rf /home/data/
将data上传到服务器/home目录下。
授权：sudo chmod 777 -R /home/data
```
#### 校验compose配置文件
进入到/home/data目录下,检查docker-compose-nacos.yml文件语法是否正确。
```
sudo docker-compose -f docker-compose-nacos.yml config
```

#### 创建nacos_config数据库
```
1、命令行创建 
docker exec -it 547a4b2c9e3f /bin/bash
mysql -h192.168.0.80 -P3306 -uroot -p123456
show databases;
CREATE DATABASE IF NOT EXISTS nacos_config default charset utf8 COLLATE utf8_general_ci;

2、导入ncos-mysql.sql文件
使用自定义数据库，首先需要创建数据库，并导入数据库脚本。
数据库脚本下载：https://github.com/alibaba/nacos/blob/develop/distribution/conf/nacos-mysql.sql
创建数据库完成后，将nacos数据库表结构导入到数据库中。数据库文件可以从nacos官网下载压缩解压后获取。

3.创建目录
sudo mkdir -p /home/data/nacos

4.创建custom.properties文件
在该目录/home/data/nacos/init.d/下创建custom.properties文件，并录入内容。
echo "management.endpoints.web.exposure.include=*" >/home/data/nacos/init.d/custom.properties

```

#### 启动服务
```
删除所有运行镜像：sudo docker rm -f $(sudo docker ps -qa)
启动服务：sudo docker-compose -f docker-compose-nacos.yml up -d
查看启动服务：sudo docker ps
```

#### 客户端测试
```
浏览器访问：http://192.168.0.80:8848/nacos/#/login， 默认 登录账号密码为nacos nacos。

```

### Nacos集群
详细安装参考：(https://starsky.blog.csdn.net/article/details/122126239)

#### 集群必要条件
> mysql已经安装完成


#### 创建nacos_config数据库
```
1、命令行创建 
docker exec -it 547a4b2c9e3f /bin/bash
mysql -h192.168.3.80 -P3306 -uroot -pDs20Pwd@
show databases;
CREATE DATABASE IF NOT EXISTS nacos_config default charset utf8 COLLATE utf8_general_ci;

```

#### 导入ncos-mysql.sql脚本
```
1、导入ncos-mysql.sql文件
使用自定义数据库，首先需要创建数据库，并导入数据库脚本。
数据库脚本下载：https://github.com/alibaba/nacos/blob/develop/distribution/conf/nacos-mysql.sql
或者使用 config/data/nacos/目录下脚本：nacos_config.sql

将下载的脚本nacos_config.sql 导入到刚才创建的nacos_config数据库中。

```

#### 创建nacos基本配置
```
for i in $(seq 1 3);\
do \
mkdir -p /home/data/nacos${i}/logs \
mkdir -p /home/data/nacos${i}/init.d \
touch /home/data/nacos${i}/init.d/custom.properties \
cat << EOF >> /home/data/nacos${i}/init.d/custom.properties
management.endpoints.web.exposure.include=*
EOF
Done
```

#### 启动服务
```
将 config/data/nacos/cluster-1.4 目录下得 nacos-cluster.yaml 上传到/home/data目录下。
执行如下命令：
sudo docker-compose -f nacos-cluster.yaml up -d

```

#### 测试验证
```
浏览器访问：http://192.168.0.80:8848/nacos/， 默认 登录账号密码为nacos nacos。

```






## Nginx

### Nginx单机版

#### 创建目录
```
删除目录：sudo rm -rf /home/data/
将data上传到服务器/home目录下。
授权：sudo chmod 777 -R /home/data
```
#### 校验compose配置文件
进入到/home/data目录下,检查docker-compose-nginx.yml文件语法是否正确。
```
sudo docker-compose -f docker-compose-nginx.yml config
```

#### 启动服务
```
删除所有运行镜像：sudo docker rm -f $(sudo docker ps -qa)
启动服务：sudo docker-compose -f docker-compose-nginx.yml up -d
查看启动服务：sudo docker ps
```

#### 登录nginx控制台
```
sudo docker exec -it nginx bash
访问浏览器测试：http://192.168.0.80:8000/

```



## vue项目部署
> 详细部署参考：https://starsky.blog.csdn.net/article/details/122211501

### 创建基本配置
```
1、创建nginx.conf配置
具体参考 config/data/vue/nginx.conf

2、创建Dockerfile
具体参考 config/data/vue/Dockerfile

3、创建docker-compose.yml模版文件
具体参考 config/data/vue/docker-compose.yml

将以上3个文件放在项目根目录下。

```

### 打包生成镜像
```
打包生成镜像时，服务器必须要安装node js，npm 等
1、登录服务器，检出vue项目，进入项目根据目录执行编译项目。
安装依赖组件：npm install
执行编译：npm run build

2、或者将以上3个文件及项目编译生成dist文件全部上传到服务器/home/web目录下。

3、执行打包： docker build -t [项目名称:版本号] [路径]
例如： sudo docker build -t demo:1.0 /home/web

```

### docker-compose启动服务
```
将以上3个文件及项目编译生成dist文件全部上传到服务器/home/web目录下。
执行启动命令： sudo docker-compose -f docker-compose.yml up -d
```

### swarm启动服务
```
1、swarm集群
 设置主节点：docker swarm init --advertise-addr 192.168.3.85 
 将工作节点加入swarm： docker swarm join --token SWMTKN-1-2o01hzyxquaiodr27h40obm66i906dhfvp002rnfm9isr5okkf-6n2ux88advdj2jqwkbb3wp7sj 192.168.3.85:2377

1、创建启动服务
执行启动命令：sudo docker service create --name dmeo -p 8888:80 --hostname dmeo --replicas 3 dmeo:1.0 

```


## 一键部署（redis/mysql/mongo/rabbitmq/nginx）

### 创建目录
```
删除目录：sudo rm -rf /home/data/
将data上传到服务器/home目录下。
授权：sudo chmod 777 -R /home/data
```
### 校验compose配置文件
进入到/home/data目录下,检查docker-compose-base.yml文件语法是否正确。
```
sudo docker-compose -f docker-compose-base.yml config
```

### 启动服务
```
删除所有运行镜像：sudo docker rm -f $(sudo docker ps -qa)
启动服务：sudo docker-compose -f docker-compose-base.yml up -d
查看启动服务：sudo docker ps
```
通过docker-compose-base.yml模版文件可以一次将redis/mysql/mongo/rabbitmq/nginx全部启动




## 实际项目业务部署
### 启动服务
```
删除所有运行镜像：sudo docker rm -f $(sudo docker ps -qa)
启动服务：sudo docker-compose -f docker-compose-bussiness.yml up -d
查看启动服务：sudo docker ps

```





## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


## 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
